image: rust:1.60

stages:
  - .pre
  - build
  - build_test_kernels
  - test
  - deploy

format:
  stage: .pre
  before_script: rustup component add rustfmt
  script: cargo fmt -- --check

.build_wasm_kernel:
  stage: build
  variables:
    KERNEL_NAME: ""
    OUTPUT_NAME: ""
  before_script:
   - rustup target add wasm32-unknown-unknown
   - cargo install cargo-make
   - apt-get update && apt-get install -y clang
  script: cargo make $KERNEL_NAME
  artifacts:
    name: ${KERNEL_NAME}.wasm
    paths:
      - target/wasm32-unknown-unknown/release/deps/$OUTPUT_NAME
    expire_in: 1 week

build_tx_kernel:
  extends: .build_wasm_kernel
  variables:
    KERNEL_NAME: wasm-tx-kernel
    OUTPUT_NAME: kernel_core.wasm

build_evm_kernel:
  extends: .build_wasm_kernel
  variables:
    KERNEL_NAME: wasm-evm-kernel
    OUTPUT_NAME: evm_kernel.wasm

# Build a test kernel set the feature flags and the name of the kernel.
# - `TEST_KERNEL_NAME` sets the name the kernel wasm file (include the `.wasm` postfix)
# - `TEST_KERNEL_FEATURES` comma separated list of features for the test kernel
#    see the `test_kernel/Cargo.toml` for a list of available features.
# NB - test kernels are built against the wee_alloc allocator, which is smaller at the
# expense of speed.
.build_wasm_test_kernel:
  stage: build_test_kernels
  variables:
    TEST_KERNEL_NAME: ""
    TEST_KERNEL_FEATURES: ""
  before_script:
    - rustup target add wasm32-unknown-unknown
    - cargo install cargo-make
    - apt-get update && apt-get install -y clang
    - echo building $TEST_KERNEL_NAME with features $TEST_KERNEL_FEATURES
  script: cargo make wasm-test-kernel $TEST_KERNEL_FEATURES
  artifacts:
    name: $TEST_KERNEL_NAME
    paths:
      - target/wasm32-unknown-unknown/release/test_kernel.wasm
    expire_in: 1 week
  when: manual

build_computation_only_test_kernel:
  extends:
    - .build_wasm_test_kernel
  variables:
    TEST_KERNEL_NAME: "basic_test_kernel.wasm"
    TEST_KERNEL_FEATURES: "none,no-alloc"

build_computation_with_alloc_test_kernel:
  extends:
    - .build_wasm_test_kernel
  variables:
    TEST_KERNEL_NAME: "basic_test_kernel.wasm"
    TEST_KERNEL_FEATURES: "none"

build_panic_test_kernel:
  extends:
    - .build_wasm_test_kernel
  variables:
    TEST_KERNEL_NAME: "panic_test_kernel.wasm"
    TEST_KERNEL_FEATURES: "test_kernel/shutdown,test_kernel/panic-counter"

build_debug_test_kernel:
  extends:
    - .build_wasm_test_kernel
  variables:
    TEST_KERNEL_NAME: "debug_test_kernel.wasm"
    TEST_KERNEL_FEATURES: "test_kernel/shutdown,test_kernel/panic-counter,test_kernel/write-debug"

build_input_test_kernel:
  extends:
    - .build_wasm_test_kernel
  variables:
    TEST_KERNEL_NAME: "input_test_kernel.wasm"
    TEST_KERNEL_FEATURES: "test_kernel/shutdown,test_kernel/panic-counter,test_kernel/write-debug,test_kernel/read-input"

build_output_test_kernel:
  extends:
    - .build_wasm_test_kernel
  variables:
    TEST_KERNEL_NAME: "output_test_kernel.wasm"
    TEST_KERNEL_FEATURES: "test_kernel/shutdown,test_kernel/panic-counter,test_kernel/write-debug,test_kernel/read-input,test_kernel/write-output"

clippy:
  stage: build
  needs:
    - build_tx_kernel
  before_script: rustup component add clippy
  script: cargo clippy -- --deny warnings

# Ensure that rebuilding the kernel is bit-for-bit identical
build_wasm_reproducible:
  stage: test
  dependencies:
    - build_tx_kernel
  needs:
    - build_tx_kernel
  before_script:
   - rustup target add wasm32-unknown-unknown
   - apt-get update && apt-get install -y clang
   - cargo install cargo-make
   - mv target/wasm32-unknown-unknown/release/deps/kernel_core.wasm ./previous.wasm
   - cargo make wasm-tx-kernel
   - cp target/wasm32-unknown-unknown/release/deps/kernel_core.wasm ./current.wasm
  script: cmp previous.wasm current.wasm

unit_tests:
  stage: test
  needs:
    - build_tx_kernel
  script: cargo test --features testing

lint_docs:
  stage: test
  needs:
    - build_tx_kernel
  script: cargo doc --workspace
  except:
    - main

pages:
  stage: deploy
  script:
    - cargo doc --workspace --no-deps --target-dir public
  artifacts:
    paths:
      - public
    expire_in: 1h
  only:
    - main
