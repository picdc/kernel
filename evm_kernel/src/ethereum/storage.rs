//! Storage interface for EVM kernel
//!
//! This interfaces to the part of Ethereum world state we keep in durable
//! storage. It does not contain any part of storage related to the tickets.
//! This module (and transactions.rs module) should be refactored completely
//! as part of milestone <https://gitlab.com/tezos/tezos/-/milestones/109>.

#[cfg(feature = "testing")]
use debug::debug_msg;
#[cfg(feature = "testing")]
use host::path::RefPath;
use host::rollup_core::RawRollupCore;
use host::runtime::Runtime;
#[cfg(feature = "testing")]
use host::runtime::ValueType;
use primitive_types::{H160, H256, U256};

/// Get the balance of an account. If a transaction is in progress,
/// this is _not_ reflected in the return value of this call.
pub fn balance<Host: Runtime>(_host: &Host, _address: H160) -> Option<U256> {
    todo!("https://gitlab.com/tezos/tezos/-/milestones/115")
}

/// Get the size of code. If no code is at that address, then this
/// function returns [None].
pub fn code_size<Host: Runtime>(host: &Host, address: H160) -> Option<U256> {
    code::<Host>(host, address).map(|x| U256::from(x.len()))
}

/// Get hash of code in storage. If some contract is being created as
/// part of a transaction that hasn't been committed yet, then this is
/// not reflected in durable storage here.
pub fn code_hash<Host: Runtime>(_host: &Host, _address: H160) -> Option<H256> {
    todo!("https://gitlab.com/tezos/tezos/-/milestones/111")
}

/// Store a program at a given address. This should both store the actual code
/// at the location, but also compute hash and length and store that as well.
#[cfg(feature = "testing")]
pub fn set_code<Host: Runtime + RawRollupCore>(
    host: &mut Host,
    _address: H160,
    value: &[u8],
) {
    const PATH: RefPath = RefPath::assert_from(b"/evm/accounts/default/code");

    match host.store_write(&PATH, value, 0usize) {
        Ok(()) => debug_msg!(Host, "Wrote code to default address"),
        Err(_) => panic!("Could not write to store"),
    }
}

/// Store a program at a given address. This should both store the actual code
/// at the location, but also compute hash and length and store that as well.
#[cfg(not(feature = "testing"))]
pub fn set_code<Host: RawRollupCore>(_host: &mut Host, _address: H160, _value: &[u8]) {
    todo!("https://gitlab.com/tezos/tezos/-/milestones/111")
}

/// Get the bytecode for a contract it has been created and committed.
#[cfg(feature = "testing")]
pub fn code<Host: Runtime>(host: &Host, _address: H160) -> Option<Vec<u8>> {
    const PATH: RefPath = RefPath::assert_from(b"/evm/accounts/default/code");

    if let Some(ValueType::Value) = host.store_has(&PATH) {
        match host.store_read(&PATH, 0, 1024) {
            Ok(value) => Some(value),
            Err(_) => panic!("Could not read store"),
        }
    } else {
        None
    }
}

/// Get the bytecode for a contract it has been created and committed.
#[cfg(not(feature = "testing"))]
pub fn code<Host: Runtime>(_host: &Host, _address: H160) -> Option<Vec<u8>> {
    todo!("https://gitlab.com/tezos/tezos/-/milestones/111")
}

/// Get a value from contract storage. If the value at this address/index
/// has been updated as part of a non-committed transaction, then this is
/// not reflected in the return value from this function.
pub fn get_value<Host: Runtime>(
    _host: &Host,
    _address: H160,
    _index: H256,
) -> Option<H256> {
    todo!("https://gitlab.com/tezos/tezos/-/milestones/109")
}

/// Set a value in contract storage. This should be called when committing
/// the toplevel transaction in contract call/createa.
pub fn set_value<Host: Runtime>(
    _host: &mut Host,
    _address: H160,
    _index: H256,
    _value: H256,
) {
    todo!("https://gitlab.com/tezos/tezos/-/milestones/109")
}
