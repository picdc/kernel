//! Precompiles for the EVM
//!
//! This module defines the set of precompiled function for the
//! EVM interpreter to use instead of calling contracts.
//! Unfortunately, we cannot use the standard `PrecompileSet`
//! provided by SputnikVM, as we require the Host type and object
//! for writing to the log.

use crate::ethereum::handler::EvmHandler;
use alloc::collections::btree_map::BTreeMap;
use debug::debug_msg;
use evm::executor::stack::{PrecompileFailure, PrecompileOutput};
use evm::Context;
use evm::ExitSucceed;
use host::rollup_core::RawRollupCore;
use primitive_types::H160;

/// Type for a single precompiled contract
pub type PrecompileFn<Host> = fn(
    _: &mut EvmHandler<Host>,
    _: &[u8],
    _: Option<u64>,
    _: &Context,
    _: bool,
) -> Result<PrecompileOutput, PrecompileFailure>;

/// Trait for encapsulating all precompiles
///
/// This is adapted from SputnikVM trait with same name. It has been
/// modified to take the Host into account, so that precompiles can
/// interact with log and durable storage and the rest of the kernel.
pub trait PrecompileSet<Host: RawRollupCore> {
    /// Execute a single contract call to a precompiled contract. Should
    /// return None (and have no effect), if there is no precompiled contract
    /// at the address given.
    fn execute(
        &self,
        handler: &mut EvmHandler<Host>,
        address: H160,
        input: &[u8],
        gas_limit: Option<u64>,
        context: &Context,
        is_static: bool,
    ) -> Option<Result<PrecompileOutput, PrecompileFailure>>;

    /// Check if there is a precompiled contract at the given address.
    fn is_precompile(&self, address: H160) -> bool;
}

/// One implementation for PrecompileSet above. Adapted from SputnikVM.
pub type PrecompileBTreeMap<Host> = BTreeMap<H160, PrecompileFn<Host>>;

impl<Host: RawRollupCore> PrecompileSet<Host> for PrecompileBTreeMap<Host> {
    fn execute(
        &self,
        handler: &mut EvmHandler<Host>,
        address: H160,
        input: &[u8],
        gas_limit: Option<u64>,
        context: &Context,
        is_static: bool,
    ) -> Option<Result<PrecompileOutput, PrecompileFailure>>
    where
        Host: RawRollupCore,
    {
        self.get(&address).map(|precompile| {
            (*precompile)(handler, input, gas_limit, context, is_static)
        })
    }

    /// Check if the given address is a precompile. Should only be called to
    /// perform the check while not executing the precompile afterward, since
    /// `execute` already performs a check internally.
    fn is_precompile(&self, address: H160) -> bool {
        self.contains_key(&address)
    }
}

/// Factory function for generating the precompileset that the EVM kernel uses.
pub fn precompile_set<Host: RawRollupCore>() -> PrecompileBTreeMap<Host> {
    let identity_precompile: PrecompileFn<Host> =
        |_: &mut EvmHandler<Host>,
         input: &[u8],
         _gas_limit: Option<u64>,
         _context: &Context,
         _is_static: bool| {
            debug_msg!(Host, "Calling identity precompile");

            Ok(PrecompileOutput {
                exit_status: ExitSucceed::Returned,
                cost: 0u64,
                output: input.to_vec(),
                logs: vec![],
            })
        };

    BTreeMap::from([(H160::from_low_u64_be(7u64), identity_precompile)])
}
