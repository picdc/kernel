//! Handle transactions
//!
//! Handle both regular ticket transactions and also EVM transactions, ie,
//! contract creation, calling contracts, and eth transfers while emulating
//! the functionality of Ethereum.

pub mod external_inbox;

/// Functionality from the transaction kernel
pub mod tx {
    pub use kernel_core::transactions::withdrawal;
}
