//! Generating arbitrary data for testing

use super::EvmTransactionCommon;
use crate::ethereum::address::*;
use crate::ethereum::basic::*;
use proptest::prelude::*;

impl EvmTransactionCommon {
    /// Generate arbitrary Ethereum transaction common data for use with testing
    pub fn arb() -> BoxedStrategy<EvmTransactionCommon> {
        (
            U256::arb(),
            any::<u8>(),
            GasPrice::arb(),
            GasLimit::arb(),
            EvmAddress::arb(),
            Wei::arb(),
            H256::arb(),
            H256::arb(),
        )
            .prop_map(|(nonce, r#type, gas_price, gas_limit, to, value, r, s)| {
                EvmTransactionCommon {
                    nonce,
                    r#type,
                    gas_price,
                    gas_limit,
                    to,
                    value,
                    r,
                    s,
                }
            })
            .boxed()
    }
}
