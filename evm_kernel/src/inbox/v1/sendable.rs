//! Construct messages for the EVM kernel
//!
//! This is an expanded version of same functionality for the Transaction
//! Kernel, and most code has been adapted from there. The only change is
//! adding EVM related transactions to the "external" kind of messages.

use crate::inbox::v1::tx::Operation;
use crate::inbox::v1::tx::ToBytesError;
use crate::inbox::v1::CompressedSignature;
use crate::inbox::v1::EvmTransactionCommon;
use kernel_core::bls::BlsKey;
use kernel_core::bls::Signature;
use tezos_encoding::enc::{self, BinError, BinErrorKind, BinWriter};
use tezos_encoding::encoding::{Encoding, HasEncoding};
use tezos_encoding::has_encoding;

pub use kernel_core::inbox::external::v1::sendable::Transaction as SendableTxTransaction;
pub use kernel_core::inbox::sendable::InternalInboxMessage;

/// EVM inbox messaages, external messages include EVM transactions.
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub enum InboxMessage {
    /// Message sent from an L1 smart-contract.
    Internal(InternalInboxMessage),
    /// Message of arbitrary bytes, in a format specific to the kernel.
    /// These bytes encode messages accepted by the transaction kernel,
    /// with only slight modification (an additional prefixed byte to
    /// identify transaction type), and EVM calls.
    External(ExternalInboxMessage),
}

/// Messages from L1 implicit accounts. Almost the same as for TX kernel,
/// but includes EVM transactions.
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub enum ExternalInboxMessage {
    /// Version 1 of external inbox messages for the EVM kernel
    V1(Batch),
}

/// Sendable batch of transactions
#[derive(Debug, PartialEq)]
pub struct Batch {
    /// All transactions in the batch. Mix of tx kernel and Ethereum
    /// transactions.
    pub transactions: Vec<Transaction>,
}

has_encoding!(Batch, SENDABLE_BATCH_ENCODING, { Encoding::Custom });

impl Batch {
    /// Create a new batch from transactions. Transactions can be both Tx kernel
    /// transactions and Ethereum transactions. Adapted from TX kernel.
    pub fn new(transactions: Vec<Transaction>) -> Self {
        Self { transactions }
    }

    fn to_bytes_signed(
        &self,
        output: &mut Vec<u8>,
    ) -> Result<Option<CompressedSignature>, ToBytesError> {
        let signed_bytes = self
            .transactions
            .iter()
            .map(|t| t.to_bytes_signed())
            .collect::<Result<Vec<(_, _)>, _>>()?;

        let signatures = signed_bytes
            .iter()
            .filter_map(|(sig, _)| sig.as_ref())
            .collect::<Vec<_>>();
        let compressed_sig = if !signatures.is_empty() {
            let sig = Signature::aggregate_sigs(signatures.as_slice())?;
            Some(CompressedSignature::from(&sig))
        } else {
            None
        };

        let output_bytes = signed_bytes
            .into_iter()
            .flat_map(|(_, bytes)| bytes)
            .collect::<Vec<u8>>();

        enc::dynamic(|output_bytes: Vec<u8>, output: &mut Vec<u8>| {
            output.extend(output_bytes);
            Ok(())
        })(output_bytes, output)?;

        Ok(compressed_sig)
    }
}

impl BinWriter for Batch {
    /// Serialize a Batch of transactions including the signature. Adapted from the
    /// transactions kernel.
    fn bin_write(&self, output: &mut Vec<u8>) -> enc::BinResult {
        let sig = self
            .to_bytes_signed(output)
            .map_err(|e| BinError::from(BinErrorKind::CustomError(format!("{}", e))))?;

        match sig {
            None => {
                output.push(0u8);
                Ok(())
            }
            Some(compressed_sig) => {
                output.push(1u8);
                compressed_sig.bin_write(output)
            }
        }
    }
}

/// Transactions for the EVM kernel. Includes TX kernel kind of transactions
/// (sequences of operations), and EVM transactions: calls, creates, and transfers
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub enum Transaction {
    /// Tx kernel transactions.
    TxTransaction(SendableTxTransaction),
    /// Ethereum transactions.
    EvmTransaction(SendableEvmTransaction),
}

impl Transaction {
    /// Create a new transactions kernel transaction.
    pub fn new_ticket_transaction(
        ops_with_keys: impl IntoIterator<Item = (Operation, BlsKey)>,
    ) -> Result<Self, ToBytesError> {
        let inner = SendableTxTransaction::new(ops_with_keys)?;
        Ok(Transaction::TxTransaction(inner))
    }

    /// Create a new transaction for calling an Ethereum contract.
    pub fn new_evm_call(
        common: EvmTransactionCommon,
        data: Vec<u8>,
    ) -> Result<Self, ToBytesError> {
        let inner = SendableEvmTransaction::EvmCall(EthereumCall { common, data });
        Ok(Transaction::EvmTransaction(inner))
    }

    /// Serialize a single transaction
    pub fn to_bytes_signed(&self) -> Result<(Option<Signature>, Vec<u8>), ToBytesError> {
        match self {
            Transaction::TxTransaction(t) => {
                let (sig, msg) = t.to_bytes_signed()?;
                let mut output = vec![0_u8];
                output.extend(msg);
                Ok((Some(sig), output))
            }
            Transaction::EvmTransaction(t) => {
                let mut output = vec![1_u8];
                t.bin_write(&mut output)?;
                Ok((None, output))
            }
        }
    }
}

/// Ethereum transactions
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub enum SendableEvmTransaction {
    /// Ethereum contract call. This also allows for transferring
    /// funds to a contract/code account.
    EvmCall(EthereumCall),
    /// Ethereum contract create. Execute the initialization code
    /// in the transaction and create the contract that is given as
    /// output from this execution (if the initialization returns
    /// normally).
    EvmCreate(EthereumCreate),
    /// Ethereum transfer between external accounts, but
    /// execute no code.
    EvmTransfer(EthereumTransfer),
}

/// Ethereum contract call. Also allows for transferring funds to the
/// contract as it is called.
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub struct EthereumCall {
    /// The common part of Ethereum transactions, see yellow paper section 4.2.
    pub common: EvmTransactionCommon,
    /// Call data passed as input to the contract call.
    #[encoding(dynamic, list)]
    pub data: Vec<u8>,
}

/// Ethereum contract create. Allows for transferring funds to the
/// newly created contract. Input data is the code that is run on
/// creation. What is returned is the code for the new contract.
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub struct EthereumCreate {
    /// The common part of Ethereum transactions, see yellow paper section 4.2.
    pub common: EvmTransactionCommon,
    /// The initialization code run before contract creation. Running this code
    /// should return the contract code that is stored in Ethereum World State.
    #[encoding(dynamic, list)]
    pub init: Vec<u8>,
}

/// Ethereum transfer. Transfer eth from one non-contract account to
/// any other account.
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub struct EthereumTransfer {
    /// The common part of Ethereum transactions, see yellow paper section 4.2.
    /// A simple Ethereum transaction contains nothing else.
    pub common: EvmTransactionCommon,
}

#[cfg(test)]
mod test {

    use proptest::collection;
    use proptest::prelude::*;
    use tezos_encoding::enc::BinWriter;

    use super::Batch;
    use super::Transaction;
    use crate::inbox::v1::tx::Operation;
    use crate::inbox::v1::EvmTransactionCommon;
    use crate::inbox::v1::ParsedEvmBatch;
    use crate::inbox::v1::ParsedEvmTransaction;
    use crate::inbox::v1::ParsedTransaction;

    proptest! {
        #[test]
        fn sendable_evm_transaction_encode_decode(
            common in EvmTransactionCommon::arb(),
            input in any::<Vec<u8>>(),
            remaining_input in any::<Vec<u8>>(),
        ) {
            // Arrange
            let transaction = Transaction::new_evm_call(common.clone(), input.clone()).unwrap();
            let mut encoded = Vec::new();
            transaction.bin_write(&mut encoded).unwrap();

            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, parsed_transaction) = ParsedTransaction::parse(encoded.as_slice())
                .expect("Parsing of encoded evm call transaction failed");

            // Assert
            if let ParsedTransaction::EvmTransaction(ParsedEvmTransaction::EvmCall(t)) = parsed_transaction {
                assert_eq!(common, t.common, "Common data part of transaction not parsed correctly");
                assert_eq!(input, t.data, "Input data to Etherem call not parsed correctly");
            } else {
                assert!(false, "Evm transaction has wrong type - not a call");
            }

            assert_eq!(remaining_input, remaining, "Parsing evm call transaction consumed too many bytes");
        }

        #[test]
        fn evm_transaction_non_signature(
            common in EvmTransactionCommon::arb(),
            input in any::<Vec<u8>>(),
        ) {
            // Arrange
            let transaction = Transaction::new_evm_call(common.clone(), input.clone()).unwrap();

            let (signature, transaction_bytes) = transaction.to_bytes_signed()?;

            let (remaining, _parsed_transaction) = ParsedTransaction::parse(transaction_bytes.as_slice())
                .expect("Parsing of evm transaction failed");

            // Assert
            assert!(signature.is_none(), "An evm transaction doesn't have a bls signature");
            assert!(remaining.is_empty(), "Parsing evm call transaction consumed too few bytes");
        }

        #[test]
        fn ticket_transaction_signature(
            operations in collection::vec(Operation::arb_with_signer(), 1..5),
        ) {
            // Arrange
            let transaction = Transaction::new_ticket_transaction(operations).unwrap();

            let (signature, transaction_bytes) = transaction.to_bytes_signed()?;

            let (remaining, _parsed_transaction) = ParsedTransaction::parse(transaction_bytes.as_slice())
                .expect("Parsing of signed transaction failed");

            // Assert
            assert!(signature.is_some(), "A ticket transaction must have signature");
            assert!(remaining.is_empty(), "Parsing evm call transaction consumed too few bytes");
        }

        #[test]
        fn sendable_ticket_transaction_encode_decode(
            operations in collection::vec(Operation::arb_with_signer(), 1..5),
            remaining_input in any::<Vec<u8>>(),
        ) {
            // Arrange
            let transaction = Transaction::new_ticket_transaction(operations).unwrap();
            let mut encoded = Vec::new();
            transaction.bin_write(&mut encoded).unwrap();

            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, parsed_transaction) = ParsedTransaction::parse(encoded.as_slice())
                .expect("Parsing of encoded ticket transaction failed");

            // Assert
            if let ParsedTransaction::TxTransaction(_t) = parsed_transaction {
                assert!(true);
            } else {
                assert!(false, "Transaction has wrong type - expected ticket transaction");
            }

            assert_eq!(remaining_input, remaining, "Parsing ticket transaction consumed too many bytes");
        }

        #[test]
        fn sendable_batch_encode_decode(
            transactions in collection::vec(
                collection::vec(Operation::arb_with_signer(), 1..5),
                1..5
            ),
            remaining_input in any::<Vec<u8>>(),
        ) {
            // Arrange
            let batch = Batch::new(
                transactions.into_iter()
                .map(|ops| Transaction::new_ticket_transaction(ops).unwrap())
                .collect()
            );

            let mut b = Vec::new();
            let sig = batch.to_bytes_signed(&mut b).unwrap();

            let mut encoded = Vec::new();
            batch.bin_write(&mut encoded).unwrap();

            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, parsed_batch) = ParsedEvmBatch::parse(encoded.as_slice())
                .expect("Parsing of encoded batch failed");

            assert_eq!(remaining_input, remaining, "Parsing batch consumed too many bytes");
            assert_eq!(sig, parsed_batch.aggregated_signature, "Expected signatures to be equal");
        }

        #[test]
        fn sendable_batch_with_evm_call(
            common in EvmTransactionCommon::arb(),
            input in any::<Vec<u8>>(),
            remaining_input in any::<Vec<u8>>(),
        ) {
            // Arrange
            let transaction = Transaction::new_evm_call(common.clone(), input.clone()).unwrap();
            let batch = Batch::new(vec![transaction]);

            let mut b = Vec::new();
            let sig = batch.to_bytes_signed(&mut b).unwrap();

            let mut encoded = Vec::new();
            batch.bin_write(&mut encoded).unwrap();

            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, parsed_batch) = ParsedEvmBatch::parse(encoded.as_slice())
                .expect("Parsing of encoded batch of evm call failed");

            assert_eq!(remaining_input, remaining, "Parsing batch consumed wrong number of bytes");
            assert_eq!(sig, parsed_batch.aggregated_signature, "Expected signatures to be equal");
        }
    }
}
