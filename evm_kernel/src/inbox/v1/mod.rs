//! All version 1 for both transaction kernel and ditto EVM kernel messages
use crate::ethereum::address::EvmAddress;
use crate::ethereum::basic::{GasLimit, GasPrice, Wei, H256, U256};
use crate::inbox::v1::tx::verifiable::VerificationError;
use crate::inbox::v1::verifiable::VerifiedTransaction;
use crate::memory::tx::Accounts;
use host::rollup_core::RawRollupCore;
use kernel_core::bls::CompressedSignature;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::combinator::map;
use nom::multi::many1;
use nom::sequence::{pair, preceded};
use tezos_encoding::enc::BinWriter;
use tezos_encoding::encoding::HasEncoding;
use tezos_encoding::nom::{dynamic, NomReader};

pub mod sendable;
#[cfg(feature = "testing")]
pub mod testing;
pub mod tx;
pub mod verifiable;

/// A de-serialized batch of transactions
///
/// Transactions may be transaction kernel
/// transactions, ie, transfers and withdrawals of tickets. They can also
/// be EVM transactions.
pub struct ParsedEvmBatch<'a> {
    /// The ordered transactions included in the batch.
    pub transactions: Vec<ParsedTransaction<'a>>,
    /// The signature for all tx kernel transactions - not the EVM kernel ones.
    /// Iff a batch has only Ethereum transactions, then this will be `None` otherwise
    /// it will be `Some(signature)`.
    pub aggregated_signature: Option<CompressedSignature>,
}

impl<'a> ParsedEvmBatch<'a> {
    /// Parse a batch, where each transaction is either *verifiable* wrt
    /// the transaction kernel, or an Ethereum transaction, which is
    /// verifiable in its own right (through its own signature verification
    /// and EVM interpretation).
    pub fn parse(input: &'a [u8]) -> tezos_encoding::nom::NomResult<Self> {
        map(
            pair(
                dynamic(many1(ParsedTransaction::parse)),
                alt((
                    map(tag([0_u8]), |_: &[u8]| {
                        Option::<CompressedSignature>::default()
                    }),
                    preceded(tag([1_u8]), map(CompressedSignature::nom_read, Some)),
                )),
            ),
            |(transactions, aggregated_signature)| ParsedEvmBatch {
                transactions,
                aggregated_signature,
            },
        )(input)
    }

    /// Verify that the batch signature is valid.
    /// TODO: <https://gitlab.com/tezos/tezos/-/issues/3711>
    pub fn verify_signature<Host: RawRollupCore>(
        &self,
        _accounts: &mut Accounts,
    ) -> Result<(), VerificationError> {
        Ok(())
    }
}

/// Transaction designator
///
/// Decide whether the following transaction is a ticket/TX transaction or
/// an Ethereum transaction.
#[derive(HasEncoding, NomReader)]
enum TransactionType {
    Tx,
    Evm,
}

/// An Ethereum transaction
///
/// For now, this only includes calls to EVM contracts, but in the future,
/// this will also include contract creation and Ethereum transfers.
/// TODO: create gitlab issue(s)
#[derive(Debug, PartialEq, Eq, Clone, HasEncoding, NomReader)]
pub enum ParsedEvmTransaction {
    /// Calling an Ethereum contract
    EvmCall(ParsedEvmCall),
    /// Create a new Ethereum contract
    EvmCreate(ParsedEvmCreate),
    /// Transfer ether from an external account to another
    EvmTransfer(ParsedEvmTransfer),
}

impl ParsedEvmTransaction {
    /// Verify that signature of transaction is correct and that caller has
    /// sufficient funds.
    /// TODO: <https://gitlab.com/tezos/tezos/-/milestones/115>
    pub fn verify(
        &self, // TODO: add account information to args to verify caller has funds
    ) -> Result<Self, VerificationError> {
        Ok(self.clone())
    }
}

/// Data common to all Ethereum transaction types
#[derive(Debug, PartialEq, Eq, Clone, Copy, HasEncoding, NomReader, BinWriter)]
pub struct EvmTransactionCommon {
    /// A scalar value equal to the number of trans- actions sent by the sender
    pub nonce: U256,
    /// EIP-2718 transaction type
    pub r#type: u8,
    /// A scalar value equal to the number of
    /// Wei to be paid per unit of gas for all computation
    /// costs incurred as a result of the execution of this
    /// transaction
    pub gas_price: GasPrice,
    /// A scalar value equal to the maximum
    /// amount of gas that should be used in executing
    /// this transaction. This is paid up-front, before any
    /// computation is done and may not be increased
    /// later
    pub gas_limit: GasLimit,
    /// The 160-bit address of the message call’s recipi-
    /// ent or, for a contract creation transaction
    pub to: EvmAddress,
    /// A scalar value equal to the number of Wei to
    /// be transferred to the message call’s recipient or,
    /// in the case of contract creation, as an endowment
    /// to the newly created account
    pub value: Wei,
    /// Signature x-axis part of point on elliptic curve. See yellow paper, appendix F
    pub r: H256,
    /// Signature, See yellow paper appendix F
    pub s: H256,
}

impl EvmTransactionCommon {
    /// Find the caller address from r and s of the common data
    /// for an Ethereum transaction, ie, what address is associated
    /// with the signature of the message.
    /// TODO <https://gitlab.com/tezos/tezos/-/milestones/115>
    pub fn caller(&self) -> EvmAddress {
        // return stub for now...
        EvmAddress::from_u64_be(0u64)
    }
}

/// A call to an EVM contract.
///
/// This is a placeholder structure that should
/// eventually contain the same fields as specified in the Ethereum Yellow
/// Paper. See issue <https://gitlab.com/tezos/tezos/-/issues/3644>
#[derive(Debug, PartialEq, Eq, Clone, HasEncoding, NomReader)]
pub struct ParsedEvmCall {
    /// Data common to all Ethereum transaction kinds
    pub common: EvmTransactionCommon,
    /// The input data to use as call data when calling the target at
    /// [common.to].
    #[encoding(dynamic, list)]
    pub data: Vec<u8>,
}

/// Create a new EVM contract
#[derive(Debug, PartialEq, Eq, Clone, HasEncoding, NomReader)]
pub struct ParsedEvmCreate {
    /// Data common to all Ethereum transaction kinds
    pub common: EvmTransactionCommon,
    /// The initial code to run. Running this code returns the code of
    /// the body of the contract being created.
    #[encoding(dynamic, list)]
    pub init: Vec<u8>,
}

/// Transfer ether between two Ethereum addresses. Sender must be an
/// external account.
#[derive(Debug, PartialEq, Eq, Clone, Copy, HasEncoding, NomReader)]
pub struct ParsedEvmTransfer {
    /// Data common to all Ethereum transaction kinds
    pub common: EvmTransactionCommon,
}

/// A single transaction for the kernel
///
/// This includes both the transactions inherited from the transactions
/// kernel, _and_ Ethereum transactions.
/// TODO: Reconsider construction of this enum so that it is better
/// balanced. Issue to come in <https://gitlab.com/tezos/tezos/-/milestones/115>.
#[derive(Debug, PartialEq, Eq)]
#[allow(clippy::large_enum_variant)]
pub enum ParsedTransaction<'a> {
    /// All transactions for the Ethereum part of the kernel.
    EvmTransaction(ParsedEvmTransaction),
    /// All transactions involving tickets, ie, ticket -transfers and
    /// -withdrawals.
    TxTransaction(tx::verifiable::VerifiableTransaction<'a>),
}

impl<'a> ParsedTransaction<'a> {
    /// Parse a single transaction, which is either a standard TX kernel
    /// transaction or an Ethereum transaction.
    pub fn parse(input: &'a [u8]) -> tezos_encoding::nom::NomResult<Self> {
        let (remaining, repr): (&'a [u8], _) = TransactionType::nom_read(input)?;

        match repr {
            TransactionType::Tx => map(
                tx::verifiable::VerifiableTransaction::parse,
                Self::TxTransaction,
            )(remaining),
            TransactionType::Evm => {
                map(ParsedEvmTransaction::nom_read, Self::EvmTransaction)(remaining)
            }
        }
    }

    /// Verify that signature is correct for transaction, either bls signature
    /// for ticket transactions, or ECDSA for Ethereum.
    /// TODO: <https://gitlab.com/tezos/tezos/-/issues/3698>
    pub fn verify(
        self,
        accounts: &Accounts,
    ) -> Result<super::v1::verifiable::VerifiedTransaction, VerificationError> {
        match self {
            ParsedTransaction::EvmTransaction(parsed_evm_transaction) => {
                Ok(VerifiedTransaction::EthereumTransaction(
                    parsed_evm_transaction.verify()?,
                ))
            }
            ParsedTransaction::TxTransaction(verifiable_transaction) => {
                Ok(VerifiedTransaction::TicketTransaction(
                    verifiable_transaction.verify(accounts)?,
                ))
            }
        }
    }
}
