//! Kernel(s) for testing PVM/WASM interpreter integration
//!
//! A single test kernel with features that can be switched on/off at build
//! time. Features are meant to be stacked in order, but this is not a hard
//! requirement (ie, do as you please).
#![cfg_attr(not(feature = "abort"), no_std)]
#![deny(missing_docs)]
#![deny(rustdoc::all)]
#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(dead_code)]
#![allow(unused_must_use)]
#![cfg(any(feature = "test-kernel", feature = "unit-kernels"))]

// Needed when using the debug_msg macro
#[cfg(not(feature = "no-alloc"))]
extern crate alloc;

#[cfg(not(feature = "no-alloc"))]
use alloc::boxed::Box;
use debug::debug_msg;
use host::input::{Input, MessageData, SlotData};
use host::rollup_core::RawRollupCore;
use host::runtime::Runtime;
use host::wasm_host::WasmHost;
use kernel::kernel_entry;

#[cfg(feature = "unit-kernels")]
pub mod unit_kernels;

/// Size of buffer for reading input using the host interface
pub const READ_BUFFER_SIZE: usize = 4096;

/// When the `panic-counter` feature is switched, the kernel will panic after
/// this many calls to `kernel_next`.
pub const MAX_ITERATIONS: u32 = 10;

/// Test kernel specific cache
///
/// `counter` is used for counting up to kernel panic. Limit is set by
/// [MAX_ITERATIONS]. `value1` and `value2` is used for doing some computation.
pub struct TestCache {
    #[cfg(not(feature = "no-alloc"))]
    counter: Box<u32>,
    #[cfg(feature = "no-alloc")]
    counter: u32,
    value1: i32,
    value2: i32,
}

impl Default for TestCache {
    fn default() -> Self {
        #[cfg(not(feature = "no-alloc"))]
        return Self {
            #[cfg(not(feature = "no-alloc"))]
            counter: Box::new(0),
            #[cfg(feature = "no-alloc")]
            counter: 0,
            value1: 0,
            value2: 1,
        };
        #[cfg(feature = "no-alloc")]
        return Self {
            counter: 0,
            value1: 0,
            value2: 1,
        };
    }
}

/// Test kernel
///
/// Entry point for the test kernel. It can read input and write output to both the
/// kernel output and log. Also, it can panic after a fixed number of iterations set
/// by [MAX_ITERATIONS]. All these behaviors are controlled by feature flags at
/// compile time.
pub fn test_kernel_run<Host: RawRollupCore>(host: &mut Host, cache: &mut TestCache) {
    let y = cache.value1 + cache.value2;

    #[cfg(feature = "panic-counter")]
    if *cache.counter > MAX_ITERATIONS {
        panic!("Counter is greater than N");
    }

    #[cfg(feature = "write-debug")]
    debug_msg!(Host, "Compute {}", y);

    #[cfg(feature = "read-input")]
    let output = {
        match host.read_input(READ_BUFFER_SIZE) {
            Some(Input::Slot(data)) => {
                #[cfg(feature = "write-debug")]
                debug_msg!(Host, "{:?}", data.as_ref());

                #[cfg(feature = "write-output")]
                host.write_output(data.as_ref());
            }
            Some(Input::Message(data)) => {
                #[cfg(feature = "write-debug")]
                debug_msg!(Host, "{:?}", data.as_ref());

                #[cfg(feature = "write-output")]
                host.write_output(data.as_ref());
            }
            None => (),
        }
    };

    #[cfg(not(feature = "no-alloc"))]
    {
        *cache.counter += 1;
    }
    #[cfg(feature = "no-alloc")]
    {
        cache.counter += 1;
    }
    cache.value1 = cache.value2;
    cache.value2 = y;

    #[cfg(feature = "abort")]
    std::process::abort()
}

use kernel::cache::Cache;
#[cfg(feature = "test-kernel")]
kernel_entry!(test_kernel_run, TestCache);
