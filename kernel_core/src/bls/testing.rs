//! Generation of Bls keys used for testing.
//!
//! Use
//! - `key in BlsKey::arb()` during Property Based Tests.
//! - `let key: BlsKey = Faker.fake()` in unit tests, when hardcoding a key is
//!   undesirable.
use super::*;
use ::fake::{Dummy, Faker};
use ::proptest::prelude::*;
use ::rand::Rng;

impl BlsKey {
    /// Generate arbitrary bls keys for testing
    pub fn arb() -> BoxedStrategy<BlsKey> {
        any::<[u8; 32]>().prop_map(Self::from_ikm).boxed()
    }
}

impl Dummy<Faker> for BlsKey {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> BlsKey {
        let ikm = rng.gen::<[u8; 32]>();
        Self::from_ikm(ikm)
    }
}
