//! Arbitrary string ticket generation.
use crate::encoding::contract::Contract;

use super::StringTicket;
use num_traits::Zero;
use proptest::prelude::*;

impl StringTicket {
    /// Strategy for generating string tickets.
    pub fn arb() -> BoxedStrategy<StringTicket> {
        (
            Contract::arb_originated(),
            String::arbitrary(),
            // non-zero
            u64::arbitrary().prop_filter("zero tickets banned", |i| !i.is_zero()),
        )
            .prop_map(|(creator, contents, amount)| {
                StringTicket::new(creator, contents, amount)
            })
            .boxed()
    }
}
