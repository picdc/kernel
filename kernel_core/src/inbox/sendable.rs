//! Constructing inbox messages for sending to the kernel.

use tezos_encoding_derive::{BinWriter, HasEncoding};

pub use super::{external::sendable::ExternalInboxMessage, InternalInboxMessage};

/// Sendable `InboxMessage`.
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub enum InboxMessage {
    /// Message sent from an L1 smart-contract.
    Internal(InternalInboxMessage),
    /// Message of arbitrary bytes, in a format specific to the kernel.
    ///
    /// The containing operation will be sent by an implicit account - but will
    /// deserialize to a structure representing *transactions* & *withdrawals* between
    /// and from **Layer2Tz4* addresses respectively.
    External(ExternalInboxMessage),
}

#[cfg(test)]
mod test {
    use super::ExternalInboxMessage;
    use super::InboxMessage;
    use crate::inbox::external::v1::sendable::{Batch, Transaction};
    use crate::inbox::external::v1::Operation;
    use crate::inbox::external::ExternalInboxMessage as ExternalInboxMessageBytes;
    use crate::inbox::external::ParsedExternalInboxMessage;
    use crate::inbox::InboxMessage as ParsedInboxMessage;
    use proptest::collection;
    use proptest::prelude::*;
    use tezos_encoding::enc::BinWriter;

    proptest! {
        #[test]
        fn sendable_v1_external_inbox_encode_decode(
            transactions in collection::vec(
                collection::vec(Operation::arb_with_signer(), 1..5),
                1..5
            ),
            remaining_input in any::<Vec<u8>>(),
        ) {
            // Arrange
            let batch = Batch::new(
                transactions.into_iter()
                            .map(|ops| Transaction::new(ops).unwrap())
                            .collect()
            );

            let inbox_message = InboxMessage::External(
                ExternalInboxMessage::V1(batch)
            );

            let mut encoded = Vec::new();
            inbox_message.bin_write(&mut encoded).unwrap();
            encoded.extend_from_slice(remaining_input.as_slice());

            // Act + Assert
            let (remaining, inbox_message) = ParsedInboxMessage::parse(encoded.as_slice())
                .expect("Parsing of inbox message failed");

            // `ExternalInboxMessage` is special, in the sense that it is encoded Variadically,
            // ie is not prefixed by any sort of length - hence we expect no remaining bytes here.
            assert!(remaining.is_empty(), "ExternalInboxMessage parsing should consume all bytes");

            if let ParsedInboxMessage::External(ExternalInboxMessageBytes(encoded)) = inbox_message {
                let (remaining, _external) = ParsedExternalInboxMessage::parse(encoded)
                    .expect("Parsing external inbox message should work");

                assert_eq!(remaining_input.as_slice(), remaining, "ParsedExternalInboxMessage::parse consumed too many bytes.")
            } else {
                panic!("Expected ExternalInboxMessage");
            }
        }
    }
}
