//! Generation of arbitrary operations for testing.
use crate::{
    bls::BlsKey,
    encoding::{entrypoint::Entrypoint, string_ticket::StringTicket},
    fake_hash::arb_kt1,
    inbox::Signer,
};

use super::{Operation, OperationContent};
use proptest::collection;
use proptest::prelude::*;

impl OperationContent {
    /// Generation strategy for withdrawal operations.
    pub fn arb_withdrawal() -> BoxedStrategy<OperationContent> {
        (arb_kt1(), StringTicket::arb(), Entrypoint::arb())
            .prop_map(|(destination, ticket, entrypoint)| {
                OperationContent::withdrawal(destination, ticket, entrypoint)
            })
            .boxed()
    }

    /// Generation strategy for transfer operations.
    pub fn arb_transfer() -> BoxedStrategy<OperationContent> {
        (BlsKey::arb(), StringTicket::arb())
            .prop_map(|(bls_account_key, ticket)| {
                OperationContent::transfer(
                    bls_account_key.public_key_hash().clone(),
                    ticket,
                )
            })
            .boxed()
    }
}

impl Operation {
    /// Generation strategy for operations.
    pub fn arb_with_signer() -> BoxedStrategy<(Operation, BlsKey)> {
        (
            BlsKey::arb(),
            i64::arbitrary().prop_map(|i| if i < 0 { -(i + 1) } else { i }),
            collection::vec(
                // Mix of withdrawals & transfers
                bool::arbitrary().prop_flat_map(|transfer| {
                    if transfer {
                        OperationContent::arb_transfer()
                    } else {
                        OperationContent::arb_withdrawal()
                    }
                }),
                0..5,
            ),
            bool::arbitrary(),
        )
            .prop_map(|(key, counter, contents, signer_as_address)| {
                let signer = if signer_as_address {
                    Signer::Layer2Address(key.public_key_hash().clone())
                } else {
                    Signer::BlsPublicKey(*key.compressed_public_key())
                };

                let op = Operation {
                    signer,
                    counter,
                    contents,
                };
                (op, key)
            })
            .boxed()
    }
}
