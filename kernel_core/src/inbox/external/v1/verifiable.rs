//! Parsing of operations, and verification that they are well-formed and signed.
use super::{Operation, OperationContent, OperationTransfer, OperationWithdraw};
use crate::bls::{BlsError, CompressedPublicKey};
use crate::encoding::string_ticket::{StringTicket, StringTicketError, TicketHashError};
use crate::inbox::external::Signer;
use crate::memory::{Account, AccountError, Accounts};
use crate::transactions::withdrawal::Withdrawal;
use alloc::collections::BTreeSet;
use crypto::hash::{Layer2Tz4Hash, TryFromPKError};
use crypto::PublicKeyWithHash;
use nom::combinator::{consumed, map};
use nom::multi::many1;
use tezos_encoding::nom::{dynamic, NomReader};
use tezos_encoding_derive::HasEncoding;
use thiserror::Error;

/// Errors that may occur when verifying a transaction.
#[derive(Debug, Error)]
pub enum VerificationError {
    /// No account currently exists at address
    #[error("No account found at address {0}")]
    NoAccountOfAddress(Layer2Tz4Hash),
    /// No public key linked to address
    #[error("No public key linked to address {0}")]
    NoPublicKeyForAddress(Layer2Tz4Hash),
    /// The expected counter did not match the actual given.
    #[error("Account operation counter at {0}, transaction had {1}")]
    InvalidOperationCounter(i64, i64),
    /// Unable to hash compressed public key.
    #[error("Unable to convert compressed public key to Layer2 address {0}")]
    AddressOfPublicKey(#[from] TryFromPKError),
    /// Invalid Ticket repr
    #[error("Invalid ticket representation {0}")]
    InvalidTicketRepr(#[from] StringTicketError),
    /// Could not identify ticket
    #[error("Unable to identify ticket {0}")]
    TicketIdentification(#[from] TicketHashError),
    /// Unable to decompress public key
    #[error("Invalid signature or public key {0}")]
    InvalidSigOrPk(#[from] BlsError),
    /// Incorrect signature
    #[error("Signature verification failed")]
    SignatureVerificationError,
    /// Operation against account would fail, if performed
    #[error("Operation against account would fail: {0}")]
    PreplayOperationError(#[from] AccountError),
    /// Transaction contained more than one operation for a given account
    #[error("Transaction contained multiple operations for account {0}")]
    MultipleOperationsForAccount(Layer2Tz4Hash),
    /// Cannot transfer from & to the same account.
    #[error("Invalid self-transfer at address {0}")]
    InvalidSelfTransfer(Layer2Tz4Hash),
}

/// Wrapper type for [Operation].
///
/// All verifiable operations within a [VerifiableTransaction] will be checked
/// to ensure that they can be applied to the signer's account correctly.
#[derive(Debug, PartialEq, Eq, HasEncoding, NomReader)]
pub struct VerifiableOperation {
    operation: Operation,
}

impl VerifiableOperation {
    fn signer(&self) -> &Signer {
        &self.operation.signer
    }

    fn verify(
        self,
        accounts: &Accounts,
        updated_accounts: &mut Accounts,
    ) -> Result<VerifiedOperation, VerificationError> {
        let Operation {
            signer,
            counter,
            contents,
        } = self.operation;

        let (mut signer_account_ref, signer_address) =
            AccountRef::from_signer(signer, accounts, updated_accounts)?;

        signer_account_ref.check_and_inc_counter(counter)?;

        let mut signer_account = signer_account_ref.deref();
        let mut withdrawals = Vec::new();

        for c in contents.into_iter() {
            match c {
                OperationContent::Withdraw(OperationWithdraw {
                    destination,
                    entrypoint,
                    ticket: ticket_repr,
                }) => {
                    let ticket = StringTicket::try_from(ticket_repr)?;
                    let identity = ticket.identify()?;
                    let amount = ticket.amount();

                    signer_account.remove_ticket(&identity, amount)?;

                    withdrawals.push(Withdrawal {
                        ticket_id: identity,
                        destination,
                        entrypoint,
                        withdrawn_amount: amount,
                    });
                }
                OperationContent::Transfer(OperationTransfer { destination, .. })
                    if destination == signer_address =>
                {
                    return Err(VerificationError::InvalidSelfTransfer(destination))
                }
                OperationContent::Transfer(OperationTransfer {
                    destination,
                    ticket: ticket_repr,
                }) => {
                    let ticket = StringTicket::try_from(ticket_repr)?;
                    let identity = ticket.identify()?;
                    let amount = ticket.amount();

                    signer_account.remove_ticket(&identity, amount)?;

                    let mut dest_account =
                        account_from_address(&destination, accounts, updated_accounts)
                            .unwrap_or_default();

                    dest_account.as_mut().add_ticket(identity, amount)?;

                    // commit update
                    match dest_account {
                        AccountRef::Cloned(dest_account) => updated_accounts
                            .add_account(destination, dest_account)
                            .expect("Account not previously updated"),
                        AccountRef::Updated(_) => (),
                    }
                }
            }
        }

        if let Some(account) = updated_accounts.account_of_mut(&signer_address) {
            *account = signer_account;
        } else {
            // Safe, as we know there is no account at `signer_address` in `updated_accounts`
            let _ = updated_accounts
                .add_account(signer_address.clone(), signer_account)
                .unwrap();
        }

        Ok(VerifiedOperation {
            address: signer_address,
            withdrawals,
        })
    }
}

/// The effects of an operation.
#[derive(Debug, PartialEq, Eq)]
pub struct VerifiedOperation {
    address: Layer2Tz4Hash,
    withdrawals: Vec<Withdrawal>,
}

/// A list of operations, each operating over a different account.
///
/// Every operation within the transaction must succeed, for the transaction
/// to succeed as a whole.
///
/// Can be used to verify the operation against it's signer and an aggregated
/// signature.
#[derive(Debug, PartialEq, Eq)]
pub struct VerifiableTransaction<'a> {
    encoded: &'a [u8],
    operations: Vec<VerifiableOperation>,
}

impl<'a> VerifiableTransaction<'a> {
    /// Parse a transaction, associating each operation with its encoding.
    pub fn parse(input: &'a [u8]) -> tezos_encoding::nom::NomResult<Self> {
        map(
            consumed(dynamic(many1(VerifiableOperation::nom_read))),
            |(encoded, operations)| VerifiableTransaction {
                encoded,
                operations,
            },
        )(input)
    }

    /// Verify that every operation in the transaction is safe to apply.
    pub fn verify(
        self,
        accounts: &Accounts,
    ) -> Result<VerifiedTransaction, VerificationError> {
        let mut updated_accounts = Accounts::default();

        let verified_ops = self
            .operations
            .into_iter()
            .map(|op| op.verify(accounts, &mut updated_accounts))
            .collect::<Result<Vec<_>, _>>()?;

        check_addresses_unique(verified_ops.iter().map(|vop| &vop.address))?;

        let withdrawals = verified_ops
            .into_iter()
            .flat_map(|vop| vop.withdrawals)
            .collect();

        Ok(VerifiedTransaction {
            updated_accounts,
            withdrawals,
        })
    }

    /// Return all signers that took part in the transaction.
    pub fn signers(&self) -> impl Iterator<Item = &Signer> {
        self.operations.iter().map(|vop| vop.signer())
    }

    /// The bytes representation of the transaction.
    pub fn encoded(&self) -> &[u8] {
        self.encoded
    }
}

/// A transaction that was correctly signed, and is guaranteed to be safe to apply.
#[derive(Debug, PartialEq, Eq)]
pub struct VerifiedTransaction {
    /// Accounts that were updated by the transaction.
    pub updated_accounts: Accounts,
    /// Withdrawals to be sent to L1.
    pub withdrawals: Vec<Withdrawal>,
}

fn check_addresses_unique<'a>(
    addresses: impl Iterator<Item = &'a Layer2Tz4Hash>,
) -> Result<(), VerificationError> {
    let mut set = BTreeSet::new();

    for address in addresses {
        if set.contains(address) {
            return Err(VerificationError::MultipleOperationsForAccount(
                address.clone(),
            ));
        }
        set.insert(address);
    }
    Ok(())
}

enum AccountRef<'a> {
    Updated(&'a mut Account),
    Cloned(Account),
}

impl<'a> AccountRef<'a> {
    fn deref(self) -> Account {
        match self {
            Self::Updated(account_ref) => account_ref.clone(),
            Self::Cloned(account) => account,
        }
    }

    fn from_signer(
        signer: Signer,
        accounts: &Accounts,
        updated_accounts: &'a mut Accounts,
    ) -> Result<(AccountRef<'a>, Layer2Tz4Hash), VerificationError> {
        Ok(match signer {
            Signer::BlsPublicKey(compressed_pk) => {
                account_from_pk(compressed_pk, accounts, updated_accounts)?
            }
            Signer::Layer2Address(address) => (
                account_from_address(&address, accounts, updated_accounts)?,
                address,
            ),
        })
    }

    fn check_and_inc_counter(&mut self, counter: i64) -> Result<(), VerificationError> {
        let expected = self.as_ref().counter();
        if expected != counter {
            return Err(VerificationError::InvalidOperationCounter(
                expected, counter,
            ));
        }

        self.as_mut().increment_counter();
        Ok(())
    }
}

impl<'a> Default for AccountRef<'a> {
    fn default() -> Self {
        Self::Cloned(Account::default())
    }
}

impl<'a> AsRef<Account> for AccountRef<'a> {
    fn as_ref(&self) -> &Account {
        match self {
            Self::Updated(u) => u,
            Self::Cloned(c) => c,
        }
    }
}

impl<'a> AsMut<Account> for AccountRef<'a> {
    fn as_mut(&mut self) -> &mut Account {
        match self {
            Self::Updated(u) => u,
            Self::Cloned(c) => c,
        }
    }
}

fn account_from_address<'a>(
    address: &Layer2Tz4Hash,
    accounts: &Accounts,
    updated_accounts: &'a mut Accounts,
) -> Result<AccountRef<'a>, VerificationError> {
    if let Some(a) = updated_accounts.account_of_mut(address) {
        Ok(AccountRef::Updated(a))
    } else {
        accounts
            .account_of(address)
            .cloned()
            .map(AccountRef::Cloned)
            .ok_or_else(|| VerificationError::NoAccountOfAddress(address.clone()))
    }
}

fn account_from_pk<'a>(
    pk: CompressedPublicKey,
    accounts: &Accounts,
    updated_accounts: &'a mut Accounts,
) -> Result<(AccountRef<'a>, Layer2Tz4Hash), VerificationError> {
    let address = pk.pk_hash()?;
    let account = account_from_address(&address, accounts, updated_accounts)?;

    Ok((account, address))
}

#[cfg(test)]
mod test {
    use proptest::collection;
    use proptest::prelude::*;
    use tezos_encoding::enc::BinWriter;
    use tezos_encoding::nom::NomReader;

    use crate::inbox::v1::Operation;

    use super::super::sendable::Transaction;
    use super::VerifiableOperation;
    use super::VerifiableTransaction;

    proptest! {
        #[test]
        fn verifiable_operation_encode_decode(
            (operation, _key) in Operation::arb_with_signer(),
            remaining_input in any::<Vec<u8>>(),
        ) {
            let mut encoded = Vec::new();
            operation.bin_write(&mut encoded).unwrap();

            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, decoded) = VerifiableOperation::nom_read(encoded.as_slice()).unwrap();
            assert_eq!(remaining_input, remaining);

            assert_eq!(operation, decoded.operation);
        }

        #[test]
        fn verifiable_transaction_encode_decode(
            ops_with_keys in collection::vec(Operation::arb_with_signer(), 1..5),
            remaining_input in any::<Vec<u8>>(),
        ) {
            let transaction = Transaction::new(ops_with_keys).unwrap();
            let (_sig, mut encoded) = transaction.to_bytes_signed().unwrap();

            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, decoded) = VerifiableTransaction::parse(encoded.as_slice()).unwrap();
            assert_eq!(remaining_input, remaining, "remaining_input should be untouched");

            let expected_operations = transaction.operations();
            let expected_operations = expected_operations.iter().collect::<Vec<_>>();

            let decoded_operations = decoded
                .operations
                .iter()
                .map(|vop| &vop.operation)
                .collect::<Vec<_>>();

            assert_eq!(expected_operations, decoded_operations);

            // Verifiable transaction contains its encoded bytes - do another decoding on this to verify.
            let (remaining, decoded_twice) = VerifiableTransaction::parse(decoded.encoded).unwrap();

            assert!(remaining.is_empty());
            assert_eq!(decoded, decoded_twice);
        }
    }
}
