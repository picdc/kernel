//! External inbox messages - transactions & withdrawals.
//!
//! External inbox messages are the mechanism by which user's interact with the kernel,
//! when either transacting between accounts on the rollup, or withdrawing from the
//! rollup.
//!
//! Mostly equivalent to TORU's [l2_batch](https://gitlab.com/tezos/tezos/-/blob/bbcb6382f1b418ff03283f7f977b32fd681f9099/src/proto_alpha/lib_protocol/tx_rollup_l2_batch.mli), the main differences being:
//! - lack of compact encoding (TODO: [tezos/tezos#3040](https://gitlab.com/tezos/tezos/-/issues/3040))
//! - replaces uses of `Ticket_hash` + `quantity` with `StringTicket`.  In future, either
//!   ticket_hash will be reintroduced, or indexes (with the compact encoding) will be encouraged.

use crypto::hash::Layer2Tz4Hash;
use nom::bytes::complete::tag;
use nom::combinator::map;
use nom::sequence::preceded;
use tezos_encoding::enc::BinWriter;
use tezos_encoding::encoding::HasEncoding;
use tezos_encoding::nom::NomReader;

use crate::bls::CompressedPublicKey;

pub mod sendable;
#[cfg(feature = "testing")]
pub mod testing;
pub mod v1;

/// External inbox message.  Defined to be arbitrary bytes by communication protocol.
///
/// To be parsed & validated into a kernel-specific data structure.
///
/// **NB** this holds a reference to the slice of the inbox message that corresponds to
/// the 'external message' part, rather than copying into a new buffer.
#[derive(Debug, PartialEq, Eq)]
pub struct ExternalInboxMessage<'a>(pub &'a [u8]);

impl BinWriter for ExternalInboxMessage<'_> {
    fn bin_write(&self, output: &mut Vec<u8>) -> tezos_encoding::enc::BinResult {
        output.extend_from_slice(self.0);
        Ok(())
    }
}

/// Upgradeable representation of external inbox messages.
#[derive(Debug, PartialEq, Eq)]
pub enum ParsedExternalInboxMessage<'a> {
    /// Version 1 of operation batching.
    V1(v1::ParsedBatch<'a>),
}

impl<'a> ParsedExternalInboxMessage<'a> {
    /// Parse an external inbox message.
    pub fn parse(input: &'a [u8]) -> tezos_encoding::nom::NomResult<Self> {
        map(
            preceded(tag([0_u8]), v1::ParsedBatch::parse),
            ParsedExternalInboxMessage::V1,
        )(input)
    }
}

/// Represents the `signer` of a layer-2 operation.
///
/// This is either a [CompressedPublicKey] or a layer-2 address, whose associated account
/// contains a corresponding BLS public key.
#[derive(Debug, Clone, PartialEq, Eq, HasEncoding, NomReader, BinWriter)]
pub enum Signer {
    /// A signer identified by a BLS public key.
    BlsPublicKey(CompressedPublicKey),
    /// A signer identified by a layer-2 address, in turn associated with a BLS public key.
    Layer2Address(Layer2Tz4Hash),
}

#[cfg(test)]
mod test {
    use super::Signer;
    use proptest::prelude::*;
    use tezos_encoding::enc::BinWriter;
    use tezos_encoding::nom::NomReader;

    proptest! {
        #[test]
        fn encode_decode_signer(signer in Signer::arb(), remaining in any::<Vec<u8>>()) {
            let mut encoded = Vec::new();
            signer
                .bin_write(&mut encoded)
                .expect("encoding should work");

            encoded.extend_from_slice(remaining.as_slice());

            let (remaining_input, decoded_signer) =
                Signer::nom_read(encoded.as_slice()).expect("decoding should work");

            assert_eq!(remaining.as_slice(), remaining_input);
            assert_eq!(signer, decoded_signer);
        }
    }
}
