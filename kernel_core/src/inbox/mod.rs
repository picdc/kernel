//! Types & encodings for the *inbox-half* of the *L1/L2 communication protocol*
//!
//! In *general*, this module is a re-implementation of the tezos-protocol
//! [inbox message repr].
//!
//! We have, however, a *specialised* [InternalInboxMessage::payload].  The communication
//! protocol a rollup to be any michelson `script_expr`, which in our case we've restricted
//! to be a `Pair ( string, string ticket )`.  See [InternalMessagePayloadRepr].
//!
//! [inbox message repr]: <https://gitlab.com/tezos/tezos/-/blob/9028b797894a5d9db38bc61a20abb793c3778316/src/proto_alpha/lib_protocol/sc_rollup_inbox_message_repr.mli>

use crypto::base58::FromBase58CheckError;
use crypto::hash::{ContractKt1Hash, HashTrait, Layer2Tz4Hash};
use nom::combinator::{map, rest};
use tezos_encoding::enc::{self, BinWriter};
use tezos_encoding::encoding::HasEncoding;
use tezos_encoding::nom::NomReader;
use thiserror::Error;

use crate::encoding::micheline::MichelineString;
use crate::encoding::michelson::MichelsonPair;
use crate::encoding::public_key_hash::PublicKeyHash;
use crate::encoding::string_ticket::{StringTicket, StringTicketError, StringTicketRepr};

pub mod external;
pub mod sendable;

pub use self::external::*;

#[derive(Debug, PartialEq, Eq, NomReader, HasEncoding)]
enum InboxMessageRepr {
    Internal(InternalInboxMessage),
    External,
}

/// Inbox message, recieved by the kernel as tezos-encoded bytes.
#[derive(Debug, PartialEq, Eq)]
pub enum InboxMessage<'a> {
    /// Message sent from an L1 smart-contract.
    Internal(InternalInboxMessage),
    /// Message of arbitrary bytes, in a format specific to the kernel.
    ///
    /// The containing operation will be sent by an implicit account - but will
    /// deserialize to a structure representing *transactions* & *withdrawals* between
    /// and from **Layer2Tz4* addresses respectively.
    External(ExternalInboxMessage<'a>),
}

impl<'a> InboxMessage<'a> {
    /// Replacement for `nom_read` for [InboxMessage].
    ///
    /// [NomReader] trait unfortunately does not propagate lifetime of the input bytes,
    /// meaning that it is impossible to use it with a type that refers to a section of
    /// the input.
    ///
    /// In our case, we want to avoid copies if possible - which require additional ticks.
    pub fn parse(input: &'a [u8]) -> tezos_encoding::nom::NomResult<Self> {
        let (remaining, repr): (&'a [u8], _) = InboxMessageRepr::nom_read(input)?;

        match repr {
            InboxMessageRepr::Internal(i) => Ok((remaining, InboxMessage::Internal(i))),
            InboxMessageRepr::External => map(rest, |ext| {
                InboxMessage::External(ExternalInboxMessage(ext))
            })(remaining),
        }
    }
}

impl BinWriter for InboxMessage<'_> {
    fn bin_write(&self, output: &mut Vec<u8>) -> tezos_encoding::enc::BinResult {
        match self {
            InboxMessage::Internal(i) => {
                enc::put_byte(&0, output);
                i.bin_write(output)
            }
            InboxMessage::External(e) => {
                enc::put_byte(&1, output);
                e.bin_write(output)
            }
        }
    }
}

/// Internal inbox message - known to be sent by an L1 smart-contract.
#[derive(Debug, PartialEq, Eq, NomReader, HasEncoding, BinWriter)]
pub struct InternalInboxMessage {
    /// Micheline-encoded payload, sent by the calling contract.
    pub payload: InternalMessagePayloadRepr,
    /// The calling smart-contract.
    pub sender: ContractKt1Hash,
    /// The originator of the transaction.
    pub source: PublicKeyHash,
}

/// Internal message payload, micheline encoded, specialised to the current kernel.
///
/// Theoretically, this may be any arbitrary (rollup-specific) micheline type.
/// The current kernel requires a pair of string (tz4 address) and a [`StringTicket`].
/// See [`InboxDeposit`].
pub type InternalMessagePayloadRepr = MichelsonPair<MichelineString, StringTicketRepr>;

/// Representation of a string-ticket deposit into the rollup.
#[derive(Debug, PartialEq, Eq)]
pub struct InboxDeposit {
    /// The destination account of the deposit.
    pub destination: Layer2Tz4Hash,
    /// The ticket - including amount - to be deposited.
    pub ticket: StringTicket,
}

/// Error converting from [InternalMessagePayloadRepr] to [InboxDeposit].
#[derive(Debug, Error)]
pub enum DepositFromInternalPayloadError {
    /// Invalid destination
    #[error("Invalid Layer2 address {0}")]
    InvalidDestination(#[from] FromBase58CheckError),
    /// Invalid Ticket repr
    #[error("Invalid ticket representation {0}")]
    InvalidTicket(#[from] StringTicketError),
}

impl TryFrom<InternalMessagePayloadRepr> for InboxDeposit {
    type Error = DepositFromInternalPayloadError;

    fn try_from(value: InternalMessagePayloadRepr) -> Result<Self, Self::Error> {
        Ok(Self {
            destination: Layer2Tz4Hash::from_b58check(value.0 .0.as_str())?,
            ticket: StringTicket::try_from(value.1)?,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::encoding::contract::Contract;

    // Ports of *internal_inbox_message* tests from
    // <https://gitlab.com/tezos/tezos/-/blob/9028b797894a5d9db38bc61a20abb793c3778316/src/proto_alpha/lib_protocol/test/unit/test_sc_rollup_management_protocol.ml>
    //
    // Needed to ensure binary-compatability with messages from Layer 1.
    use super::*;

    #[test]
    fn test_encode_decode_internal_inbox_message() {
        // binary encoding produced by lightly-modified (to print encoded data) protocol test
        // representing a `Pair ( string, string ticket )`
        let expected_bytes = vec![
            // Inbox message start
            0, // Internal tag
            // Payload
            7, // Prim_2
            7, // Pair tag,
            1, // String tag
            0, 0, 0, b'$', // String size
            b't', b'z', b'4', b'M', b'S', b'f', b'Z', b's', b'n', b'6',
            b'k', // tz4 (1)
            b'M', b'D', b'c', b'z', b'S', b'h', b'y', b'8', b'P', b'M',
            b'e', // tz4 (2)
            b'B', b'6', b'2', b'8', b'T', b'N', b'u', b'k', b'n', b'9',
            b'h', // tz4 (3)
            b'i', b'2', b'K',  // tz3 (4)
            7,     // Prim_2
            7,     // Pair tag
            b'\n', // Bytes tag
            0, 0, 0, 22, // Bytes length
            // KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq
            1, 209, 163, b'|', 8, 138, 18, b'!', 182, b'6', 187, b'_', 204, 179, b'^', 5,
            24, 16, b'8', 186, b'|', // kt1 end
            0,    // Padding
            7,    // Prim_2
            7,    // Pair
            1,    // String tag
            0, 0, 0, 3, // String size
            b'r', b'e', b'd', // string contents
            0,    // int encoding tag
            1,    // amount
            // Sender KT1BuEZtb68c1Q4yjtckcNjGELqWt56Xyesc - contract hash
            b'$', b'f', b'g', 169, b'1', 254, 11, 210, 251, 028, 182, 4, 247, 020, b'`',
            30, 136, b'(', b'E', b'P', // end kt1
            // Source tz1RjtZUVeLhADFHDL8UwDZA6vjWWhojpu5w
            0, // PKH - Ed25519 tag
            b'B', 236, b'v', b'_', b'\'', 0, 019, b'N', 158, 14, 254, 137, 208, b'3',
            142, b'.', 132, b'<', b'S', 220, // end tz1
        ];

        let l2_address = "tz4MSfZsn6kMDczShy8PMeB628TNukn9hi2K".into();

        let ticket_creator =
            Contract::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq")
                .expect("valid Kt1 address");

        let string_ticket = StringTicket::new(ticket_creator, "red".into(), 1);

        let sender =
            ContractKt1Hash::from_b58check("KT1BuEZtb68c1Q4yjtckcNjGELqWt56Xyesc")
                .expect("valid Kt1 address");

        let source = PublicKeyHash::from_b58check("tz1RjtZUVeLhADFHDL8UwDZA6vjWWhojpu5w")
            .expect("valid tz1 address");

        let inbox_message = InboxMessage::Internal(InternalInboxMessage {
            payload: MichelsonPair(MichelineString(l2_address), string_ticket.into()),
            sender,
            source,
        });

        // Encoding
        let mut bin = Vec::new();
        inbox_message
            .bin_write(&mut bin)
            .expect("serialization should succeed");

        assert_eq!(expected_bytes, bin, "error in serialization");

        // Decoding
        let (input_remaining, parsed_message) =
            InboxMessage::parse(bin.as_slice()).expect("deserialization should work");

        assert!(input_remaining.is_empty());
        assert_eq!(inbox_message, parsed_message, "error in deserialization");
    }

    #[test]
    fn test_encode_decode_external_inbox_message() {
        // The prefix consists of 1 bytes:
        // - Byte 0 is the tag (0 for internal, 1 for external).
        let assert_enc_with_prefix = |message: Vec<u8>| {
            let inbox_message =
                InboxMessage::External(ExternalInboxMessage(message.as_slice()));

            let mut real_encoding = Vec::new();
            assert!(inbox_message.bin_write(&mut real_encoding).is_ok());

            let real_prefix = real_encoding[0];

            let mut expected_encoding = vec![1];
            expected_encoding.extend_from_slice(message.as_slice());

            assert_encode_decode_inbox_message(inbox_message);
            assert_eq!(1_u8, real_prefix);
            assert_eq!(expected_encoding, real_encoding);
        };

        assert_enc_with_prefix(vec![]);
        assert_enc_with_prefix(vec![b'A']);
        assert_enc_with_prefix("0123456789".as_bytes().to_vec());
        assert_enc_with_prefix(vec![b'B'; 256]);
        assert_enc_with_prefix(vec![b'\n'; 1234567]);
        // Contents should not impact the prefix
        assert_enc_with_prefix(vec![5; 1234567]);
    }

    fn assert_encode_decode_inbox_message(message: InboxMessage) {
        let mut encoded = Vec::new();
        assert!(message.bin_write(&mut encoded).is_ok());

        let decoded = InboxMessage::parse(encoded.as_slice())
            .expect("Deserialization failed")
            .1;

        assert_eq!(message, decoded);
    }
}
